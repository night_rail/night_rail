import json
import importlib

session_name = "sessionNabu"
aiguillage_module_file = session_name + "_aiguillage"
aiguillage = importlib.import_module(aiguillage_module_file, package=".")
# aiguillage = __import__(aiguillage_module_file)

# load an existing configuration into a dictionary
with open(aiguillage.Parameters.json_filename, "r") as fp:
    json_conf = json.load(fp)

# change a parameter using a fragment of its name
name_fragment = "BlissFir"  # full : WorkFlowScheme_DatasetLocation_BlissFirstScan
# if not found or several matches (>1) are found an exception will be raised
aiguillage.Parameters.search_and_change(name_fragment="BlissFir", value="my_new_scan.h5")

# set the json dictionary with updated parameters
aiguillage.Parameters.change_dic_by_parameters(json_conf)

# write another ( or possible the same) json file with an update conf


with open("new_conf.json", "w") as fp:
    json_conf = json.dump(json_conf, fp, indent=4)  # withoud indent it will not be very readable
