import argparse
import re
import logging
from .complete_path import Completer
import readline
import json
import networkx as nx
import re
import os
from functools import reduce
import operator
import shutil
import copy
import textwrap
import time


logger = logging.getLogger(__name__)
logger.setLevel("INFO")

environment_variables = {}


def get_python_preamble():
    s = "#!/usr/bin/env python\n"
    s += "import os\n"
    for var_name, var_value in environment_variables.items():
        s += f"""os.environ["{var_name}"]="{var_value}" \n"""
    return s

def sequence_of_typed(cls, lmin, lmax):
    class Foo(list):
        l_min = lmin
        l_max = lmax
        my_cls = cls

        def __init__(self, value):
            value_list = [self.my_cls(tok) for tok in value.split(" ") if len(tok.strip(" "))]
            self.clear()
            for t in value_list:
                self.append(t)

            if self.l_min and len(self) < self.l_min:
                raise ValueError(f" the list must comprise at least {self.l_min} values")

            if self.l_max and len(self) > self.l_max:
                raise ValueError(f" the list must comprise at most {self.l_max} values")

    return Foo


def limited_typed_class(cls, lmin, lmax):
    class Foo(cls):
        vmin = lmin
        vmax = lmax
        my_cls = cls

        def __new__(self, value):
            return self.my_cls.__new__(self, value)

        def __init__(self, value):
            self.my_cls.__init__(value)
            if self.vmax is not None:
                if self > self.vmax:
                    raise ValueError(f" value {self} beyond limits {self.vmin, self.vmax}")
            if self.vmin is not None:
                if self < self.vmin:
                    raise ValueError(f" value {self} beyond limits {self.vmin, self.vmax}")

    return Foo


class ContiguousString(str):
    def __init__(self, value):
        pattern = r"[^\_a-z0-9A-Z]"
        if re.search(pattern, str(value)):
            # Character other then _ a-z A_Z 0-9 was found
            raise ValueError(f"Character other then _ a-z A_Z 0-9 was found  it was {str(value)}")


# template/base classes
class SerialiseForward(dict):
    just_one_shot = False # to pevent the just one spare question mechanism used by the defaults query in the gui    
    def __init__(self):
        self.nodes = []
        self.is_root = True

    def set_class_name(self, name):
        self["class_name"] = name

    def set_question_answer_dict(self, question_answer_dict):
        self["question_answer_dict"] = question_answer_dict

    def set_completion_history(self, completion_history):
        self["completion_history"] = completion_history

    def make_new_node(self):
        new_node = SerialiseForward()
        new_node.is_root = False
        cardinal = len(self.nodes)
        self.nodes.append(new_node)
        self[f"node_{cardinal:04}"] = new_node
        return new_node

    def set_global_histories(self, global_histories):
        self["global_histories"] = global_histories


def without_print_of_default_value(question):
    if "(Tab will reset to default value" in question:
        return question[: question.find("(Tab will reset to default value")   ]
    else:
        return question
    
class SerialiseBackward(dict):
    just_one_shot = False # to prevent the just one spare question mechanism used by the defaults query in the gui        
    imtop = False

    def __init__(self, initial_dict, top_dict=None, automatic=False):
        self.automatic = automatic

        self.top_dict = top_dict
        # the top dict is kept and propagated to all 
        # the instantiated sub serialiser for all the sub branche
        # It turns useful in case of problems, it allows to come back
        # and repeat dialogues

        self.update(initial_dict)

    def serialiser_backward_instantiation(self, initial_dict):
        # instantiation with propagation of things to be propagated
        return SerialiseBackward(initial_dict, top_dict=self.top_dict, automatic=self.automatic)

    def get_previous_answer(self, question, class_name):

        if question == without_print_of_default_value(question) :
            if question in self["question_answer_dict"]:
                return self["question_answer_dict"][question]
        else:
            for qq in self["question_answer_dict"].keys():
                if without_print_of_default_value(question) == without_print_of_default_value(qq):
                    return self["question_answer_dict"][qq]
        
        if True:
            # Recovery mode: this happens opening old json files after a modification in the dialogue structures
            logger.warning(" RECOVERY MODE:  it seems that we are dealing with an old configuration file")
            recovery_dict = self.serialiser_backward_instantiation(self.top_dict)
            my_answer = recovery_dict.recovery_search_question(question)
            if not my_answer:
                recovery_dict = self.get_subnode_by_class_name(class_name)
                my_answer = recovery_dict.recovery_search_question(question) if recovery_dict else ""
            acceptable_choices = split_in_possible_choices(question)
            if len(acceptable_choices) == 0 or my_answer in acceptable_choices:
                return my_answer
            else:
                logger.warning(f" RECOVERY MODE FAILURE: I am retrieving {my_answer} which is not in {acceptable_choices}. It seems that the options names have changed. Now returning the first acceptable one")
                return list(acceptable_choices.keys())[0]

    def get_subnode_by_class_name(self, class_name):
        recovery_dict = self.serialiser_backward_instantiation(self.top_dict)
        recovery = recovery_dict.recovery_search_class(class_name)
        if recovery:
            return recovery
        else:
            return None

    def recovery_search_question(self, question):
        if question in self["question_answer_dict"]:
            return self["question_answer_dict"][question]
        for k in self.keys():
            if k.startswith("node_"):
                test = self.serialiser_backward_instantiation(self.get(k))
                test = test.recovery_search_question(question)
                if test is not None:
                    return test
        return None

    def recovery_search_class(self, class_name):
        if class_name == self["class_name"]:
            return self
        for k in self.keys():
            if k.startswith("node_"):
                test = self.serialiser_backward_instantiation(self.get(k))
                test = test.recovery_search_class(class_name)
                if test is not None:
                    return test
        return None

    def get_completion_history(self):
        return self["completion_history"]

    def get_subnode(self, inode, class_name=None):
        tag = f"node_{inode:04}"
        sub_dict = self.get(tag, None)
        if sub_dict is not None:
            return self.serialiser_backward_instantiation(sub_dict)
        else:
            msg = f"""
            >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            The branch is missing the required tag {tag}. For class {class_name}
            Probably we are reading a file created by an older version.
            We are going to search it based on the parameter class
            """
            logger.warning(msg)
            found = self.get_subnode_by_class_name(class_name)

            if not found:
                msg = f"""
                >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                The search by classname has not given results.
                Now we will ask to the user
                """
                logger.warning(msg)
                self[tag] = FromUser(in_replacement=True, automatic=self.automatic)
                return self[tag]
            else:
                return found

        
class FromDefault:
    """in place of a serialiser instance, will force applying default values"""

    def get_subnode(self, inode, class_name = None):
        return self


class FromUser:
    """in place of a serialiser instance, will ask questions"""

    # this allows to know how deep we are in the tree
    my_current_level = 0
    # and this to skip a section
    now_skipping_to_level = None

    punctiliousness_table = {"predefined":0, "simple":1, "advanced":2}

    question_punctiliousness_controller = {}

    just_one_shot = False # to install a just one spare question mechanism used by the defaults query in the gui
    
    def __init__(self, in_replacement=False, skip_to=None, go_to=None, predefined_sequence = None, predefined_dict = None,
                 predefined_session_name = None, question_punctiliousness_controller = {},test_variables_dict = None,
                 check_for_path=True, just_one_shot=False,
                 automatic=False
    ):
        
        self.skip_to = skip_to
        self.go_to = go_to
        self.automatic=automatic
        self.in_replacement = in_replacement
        self.predefined_sequence = copy.copy(predefined_sequence)
        self.predefined_dict = copy.copy(predefined_dict)
        self.predefined_session_name = predefined_session_name
        self.question_punctiliousness_controller = question_punctiliousness_controller

        if automatic:
            self.check_for_path = False
        else:
            self.check_for_path = check_for_path
            

        
        self.just_one_shot=just_one_shot
        if test_variables_dict is not None:
            self.test_variables_dict  = flatten_dict(test_variables_dict) #  only the selected variable in this dictionary ( variables whose name appear as  a dictionary  key pointing to a True value)
            # are determined by a dialog, all the other will take the default value
        else:
            self.test_variables_dict  =  None
            
    def punctiliousness_level_is_less_than_parameter_one(self, par_obj, level ) :
        assert ( (not level) or level in self.punctiliousness_table), "this should not happen"
        handle = par_obj.punctiliousness_handle
        punct_offset = par_obj.punctiliousness_offset
        
        if not ( handle in self.question_punctiliousness_controller ) :
            return True
        else:
            lut = self.punctiliousness_table
            return lut[level] + punct_offset < lut[self.question_punctiliousness_controller[handle]]
        
    def retouch_question(self, question):
        if self.in_replacement:
            extra = "This was not present in the saved configuration. Probably you are loading an old configuration"
            question = extra + question
        question = question + "\n" + " " * 8
        return question

    def get_subnode(self, inode, class_name = None):
        return self

    def input_from_question_answer_and_autocompletion(
        self,
        question,
        default="",
        type_to=False,
        typed_default=None,
        history=[],
        path_is_dir=False,
        path_is_file=False,
        create_dir_on_demand=False,
        asker = None
    ):
        question = self.retouch_question(question)


        suggestion = None    
        if self.predefined_sequence:
            suggestion = self.predefined_sequence[0]
            self.predefined_sequence = self.predefined_sequence[1:]

        if self.predefined_dict and asker:
            if asker.__class__ in self.predefined_dict:
                suggestion = self.predefined_dict[asker.__class__]

        if suggestion:
            default = suggestion
            typed_default = suggestion
    
        ## The __init__ function of Completer registers self.complete ( of comp object below)  into the readline library.
        ## That is how the mechanism is activated
        comp = Completer(history, type_to=type_to, typed_default=typed_default)

        got_answer = False
        while not got_answer:

            if self.predefined_session_name:
                if question.startswith(SessionNamePar.asked_question):
                    result = self.predefined_session_name
                    break

        
            # if the asker object is configured on advanced level then we 
            # skip the block below

            if not  self.punctiliousness_level_is_less_than_parameter_one( asker, "simple"  ) :
                if suggestion:
                    print(f" SKIPPING because of a predefinition: {question}   --> {suggestion}")
                    result = suggestion
                    break

            if self.test_variables_dict is not None:
                my_class_name = asker.__class__.__name__
                if not self.test_variables_dict.get(my_class_name, False):
                    print(f" {my_class_name} not Test Variable. Applying previous default  : {question}   --> {default}")
                    result = default
                    break
                else:
                    pass # it will be asked
                
            if self.now_skipping_to_level and self.now_skipping_to_level < self.my_current_level:
                print(f" SKIPPING : {question}   --> {default}")
                result = default
                break
            else:
                self.now_skipping_to_level = None


            # if the parameter object "asker" is configured as predefined
            # then we take the default and we skip
            if not self.punctiliousness_level_is_less_than_parameter_one(asker,  "predefined") :
                if default:
                    print(f" SKIPPING because of punctiliousness level from handle **{asker.punctiliousness_handle}**: {question}   --> {default}")
                    result = default
                    break
            
            readline.set_startup_hook(lambda: readline.insert_text(default))

            try:
                result = input(question)
            finally:
                readline.set_startup_hook()

            result = result.strip()

            if result == "ff":
                result = default
                self.now_skipping_to_level = self.my_current_level - 1

            if result == "ee":
                result = default
                self.now_skipping_to_level = self.my_current_level - 100000

            got_answer = True
            if type_to:
                try:
                    tmp = type_to(result)
                except ValueError as exc:
                    got_answer = False
                    extra_message = ""
                    if hasattr(type_to, "vmin") and hasattr(type_to, "vmax"):
                        extra_message = f"It seems that the input must be limited between {type_to.vmin} and {type_to.vmax} and of class {type_to.my_cls} "
                    logger.warning(
                        f"""The answer that you gave "{result}" was not of type {type_to}. {extra_message}"""
                    )
            else:
                message = ""
                if self.check_for_path and (path_is_dir or path_is_file):
                    if not os.path.exists(result):
                        if path_is_dir and create_dir_on_demand:
                            try:
                                os.makedirs(result, exist_ok=False)
                            except OSError:
                                message = f""" The path {result} could not be created"""
                        else:
                            message = f""" The path {result} does not exists, but  was required to"""
                    else:
                        if path_is_dir:
                            if not os.path.isdir(result):
                                message = (
                                    f""" The path {result} that you provided is not a directory, but was required to"""
                                )
                        if path_is_file:
                            if not os.path.isfile(result):
                                message = f""" The path {result} that you provided is not a file, but was required to"""
                    if len(message):
                        got_answer = False
                        logger.warning(message)
                        continue

        return result, comp.get_history()

    def input_from_question_answer(self, question="question for the [U]ser", default="", type=str, asker = None):
        question = self.retouch_question(question)
        default = default.lower()
        possible_choices = split_in_possible_choices(question)
        got_answer = False
        while not got_answer:
            if (default != "") and (default not in possible_choices):
                logger.warning(
                    f"""The default value "{default}" was not between the possible choices {list(possible_choices.keys())}. Now setting it to empty string"""
                )
                default = ""
            shown_question = question + f":[{default}]"


            if self.predefined_session_name:
                if question.startswith(SessionNamePar.asked_question):
                    answer = self.predefined_session_name
                    break
            
            if self.predefined_sequence:
                answer = self.predefined_sequence[0]
                self.predefined_sequence = self.predefined_sequence[1:]
                break


            if self.predefined_dict and asker:
                # print( " checking if ", asker.__class__ , " in ", list(self.predefined_dict.keys()) ) 
                if asker.__class__ in self.predefined_dict:
                    answer = self.predefined_dict[asker.__class__]
                    print(f" SKIPPING because of predefined dictionary: {question}   --> {answer}")
                    break


            if self.test_variables_dict is not None:
                my_class_name = asker.__class__.__name__
                if not self.test_variables_dict.get(my_class_name, False):
                    print(f" {my_class_name} not Test Variable. Applying previous default  : {question}   --> {default}")
                    answer = default
                    break
                else:
                    pass # it will be asked
                
            # if the parameter object "asker" is configured as predefined
            # then we take the default and we skip
            if not self.punctiliousness_level_is_less_than_parameter_one(asker,  "predefined") :
                print(f" SKIPPING because of punctiliousness level from handle **{asker.punctiliousness_handle}**: {question}   --> {default}")
                answer= default
                break
                
            
            if self.now_skipping_to_level and self.now_skipping_to_level < self.my_current_level:
                answer = default
                break
            else:
                self.now_skipping_to_level = None

            answer = input(shown_question).lower()
            if answer == "ff":
                answer = default
                self.now_skipping_to_level = self.my_current_level + 1
            if answer == "ee":
                answer = default
                self.now_skipping_to_level = self.my_current_level + 10000

            if answer == "":
                answer = default
            if answer in possible_choices:
                answer = possible_choices[answer]
                got_answer = True
            else:
                logger.warning(
                    f"""The answer that you gave "{answer}" was not between the possible choices {list(possible_choices.keys())}. """
                )
        return answer


class BaseScriptsContainer(list):
    def b_dump_scripts(self, session_name="", version="1.0"):
        for scrpt in self:
            scrpt.b_finalise_script()

        non_empty_scripts = [scrpt for scrpt in self if scrpt.b_get_script() is not None]

        if len(non_empty_scripts) == 1:
            mono_task = True
            dummy = DummyScript()
            dummy.b_finalise_script()
            non_empty_scripts.append(dummy)
        else:
            mono_task = False

        G = nx.Graph()
        edges = []

        script_afferents = {}
        for scrpt_1 in non_empty_scripts:
            for scrpt_2 in non_empty_scripts:
                my_afferent_scripts = script_afferents.get(scrpt_2, [])

                if scrpt_1 is not scrpt_2:
                    intersection = set(scrpt_1.b_provided_outputs) & set(scrpt_2.b_required_inputs)
                    if len(intersection):
                        edges.append((scrpt_1, scrpt_2))

                        my_afferent_scripts.append(scrpt_1)

                script_afferents[scrpt_2] = my_afferent_scripts
        if mono_task:
            lonely_script = non_empty_scripts[0]
            script_afferents[dummy] = [lonely_script]
            edges.append((lonely_script, dummy))

        DG = nx.DiGraph(edges)
        scripts_to_write = list(nx.topological_sort(DG))

        scripts_names = {}
        script_by_name = {}
        
        for ii, scrpt in enumerate(scripts_to_write):
            name = f"{session_name}_{ii:03d}_{scrpt.__class__.__name__}.nrs"
            scripts_names[scrpt] = name
            script_by_name[name] = scrpt
            script_txt_lines = scrpt.b_get_script(script_name=scripts_names[scrpt])

            with open(scripts_names[scrpt], "w") as f:
                f.write(script_txt_lines)  # scripts are written here
            os.chmod(scripts_names[scrpt], 0o744)

        execution_dependencies = {}  # to be written
        for ii, my_scrpt in enumerate(scripts_to_write):
            my_name = scripts_names[my_scrpt]
            deps = [scripts_names[other_scrpt] for other_scrpt in script_afferents[my_scrpt]]
            execution_dependencies[my_name] = deps

        scripts_outputs = {}  # to be written
        scripts_inputs = {}  # to be written

        
        for my_scrpt in scripts_to_write:
            
            my_name = scripts_names[my_scrpt]
                            
            scripts_outputs[my_name] = my_scrpt.b_provided_outputs
            scripts_inputs[my_name] = my_scrpt.b_required_inputs

        json_name_metadata = session_name + "_nr_metadata.json"
        BaseScript.move_list_for_processed_dir.update([json_name_metadata])

        json_name_configuration = session_name + "_configuration.json"
        BaseScript.move_list_for_processed_dir.update([json_name_configuration])

        with open(f"{session_name}.nrd", "w") as fp:
            json.dump(execution_dependencies, fp, indent=4)
        with open(f"{session_name}.nri", "w") as fp:
            json.dump(scripts_inputs, fp, indent=4)
        with open(f"{session_name}.nro", "w") as fp:
            to_be_moved = set(BaseScript.move_list_for_processed_dir)

            to_be_moved.update([scripts_names[my_scrpt] for my_scrpt in scripts_to_write])

            BaseScript.to_be_moved = list(set(list(to_be_moved)))

            scripts_outputs.update({"to_be_moved": list(to_be_moved)})
            json.dump(scripts_outputs, fp, indent=4)

        nodes = []
        for my_scrpt in scripts_to_write:
            my_name = scripts_names[my_scrpt]
            nodes.append({"task_type": "script", "task_identifier": os.path.abspath(my_name), "id": my_name})

        links = []
        for target, sources in execution_dependencies.items():
            my_required = set(scripts_inputs[target])
            for src in sources:
                links.append({"source": src, "target": target})
                my_required = my_required - set(scripts_outputs[src])
            if len(my_required):
                logger.warning(
                    f"WARNING from the available information given, for the script {target} the following dependencies do not seem to be provided by any other script: {my_required}  "
                )

        ewoks_wf_dict = {}

        ewoks_wf_dict["links"] = links

        ewoks_wf_dict["nodes"] = nodes

        ewoks_wf_dict["graph"] = {"id": session_name, "schema_version": "1.0"}

        with open(f"{session_name}_ewoks_wf.nrw", "w") as fp:
            json.dump(ewoks_wf_dict, fp, indent=4)

        return script_by_name

def class2name(cls):
    name = cls.__name__
    name = "".join([char if char.islower() else "_" + char.lower() for char in name])
    if name[0] == "_":
        name = name[1:]
    return name


class BaseScript:
    b_im_python_script = False
    registrable_key_val_dict = {}
    move_list_for_processed_dir = set()
    slurm_string = ""

    def __init__(self):
        self.b_neuralgic_for_start_listening_list = {}
        self.b_neuralgic_listening_list = {}
        self.b_neuralgic_listening_class_list = set()
        self.b_neuralgic_for_stop_listening_list = {}
        self.b_reset()

    def b_add_to_classes_of_interest(self, classes_of_interest):
        for cls in classes_of_interest:
            var_name = class2name(cls)
            setattr(self, var_name, None)

        self.b_neuralgic_listening_class_list.update(classes_of_interest)

    def b_add_to_neuralgic_listening_list(self, neuralgic_ids_to_func_dict):
        self.b_neuralgic_listening_list.update(neuralgic_ids_to_func_dict)

    def b_add_to_neuralgic_for_start_listening_list(self, neuralgic_ids_to_func_dict):
        self.b_neuralgic_for_start_listening_list.update(neuralgic_ids_to_func_dict)

    def b_add_to_neuralgic_for_stop_listening_list(self, neuralgic_ids_to_func_dict):
        self.b_neuralgic_for_stop_listening_list.update(neuralgic_ids_to_func_dict)

    @classmethod
    def b_compose_output_name(cls, postfix=".nx"):
        result = BasePar.b_auxiliary_global_parameters["session_name"]
        result = result + "_" + cls.__name__ + "_" + postfix
        return result

    def b_get_script(self, script_name=""):
        if self.b_im_python_script:
            return self.b_py_script
        else:
            return self.b_get_script_bash(script_name)

    def b_get_script_bash(self, script_name=""):
        if not len(self.b_script_lines):
            return None
        result = "#!/bin/bash\n"
        result = result + self.slurm_string + "\n"

        for var_name, var_value in environment_variables.items():
            result += f"""echo "Setting environment variable {var_name} to {var_value} "\n """
            result += f"""export {var_name}="{var_value}" \n """

        result += f"""echo "Running script {script_name} "\n """

        for ii, line in enumerate(self.b_script_lines):
            safe_line = ""
            for tok in line.split("\n"):
                safe_line += " " + tok + "\n"
            result += f"## Instruction N. {ii:03d}\n"
            result += rf"{safe_line}"  # double new-line is important in case the line ends with \ which would merge with the following line
            result += f"\n\nerror_code=$?\n"
            result += f"if (( $error_code != 0 )) ; then \n"
            result += f"""  echo  "ERROR at instruction  N.  {ii:03d} script_name {script_name}"    \n"""
            result += f"    exit $error_code\n"
            result += f"fi \n"
            result += f"\n"

        return result

    def b_reset(self):
        self.b_reset_script_empty()
        self.b_required_inputs = []
        self.b_provided_outputs = []
        self.b_py_script = None
        self.proper_init()

    def b_reset_script_empty(self):
        self.b_script_lines = []

    def b_set_slurm_string(self, slurm_string):
        self.slurm_string = slurm_string

    def b_add_script_line(self, line):
        assert not self.b_im_python_script
        self.b_script_lines.append(line)

    def b_set_python_script(self, py_script):
        assert self.b_im_python_script
        self.b_py_script = py_script

    def b_extend_required_inputs(self, required_inputs=[]):
        if not isinstance(required_inputs, (list, tuple)):
            raise ValueError("the argument must be either a list or a tuple of strings")
        self.b_required_inputs.extend(required_inputs)

    def b_extend_provided_outputs(self, provided_outputs=[]):
        if not isinstance(provided_outputs, (list, tuple)):
            raise ValueError("the argument must be either a list or a tuple of strings")
        self.b_provided_outputs.extend(provided_outputs)

    # def b_set_script(self, script=""):
    #     self.b_script = script

    def b_start_considering_neuralgic_node(self, par_obj):
        assert isinstance(par_obj, BasePar)
        my_foo = self.b_neuralgic_for_start_listening_list.get(par_obj.b_neuralgic_id, self.b_nothing_to_do)
        my_foo(par_obj)

    def b_consider_neuralgic_node(self, par_obj):
        assert isinstance(par_obj, BasePar)
        my_foo = self.b_neuralgic_listening_list.get(par_obj.b_neuralgic_id, self.b_nothing_to_do)
        my_foo(par_obj)

        if par_obj.__class__ in self.b_neuralgic_listening_class_list:
            self.b_consider_obj_of_interesting_class(par_obj)

    def b_consider_obj_of_interesting_class(self, par_obj):
        name = class2name(par_obj.__class__)
        setattr(self, name, par_obj.value)

    def b_stop_considering_neuralgic_node(self, par_obj):
        assert isinstance(par_obj, BasePar)
        my_foo = self.b_neuralgic_for_stop_listening_list.get(par_obj.b_neuralgic_id, self.b_nothing_to_do)
        my_foo(par_obj)

    def b_nothing_to_do(self, par_obj):
        pass

    def b_register_for_processed_dir(self, file_dir_list):
        assert isinstance(file_dir_list, (list, tuple, set)), "argument must be a list or a tuple or a set"

        self.move_list_for_processed_dir.update(file_dir_list)

    def b_consider_external_file_for_storing(self, external_file):
        result = BasePar.b_auxiliary_global_parameters["session_name"]
        result = result + "_" + self.__class__.__name__ + "_" + os.path.basename(str(external_file))
        if not os.path.isfile(str(external_file)):
            logger.warning(f"Could not find file {external_file} for storing")
        else:
            shutil.copyfile(external_file, result) 
            self.b_register_for_processed_dir([result])

    @classmethod
    def b_class_set_used(cls, value):
        cls.b_class_register_key_val("im_used", value)

    @classmethod
    def b_class_is_used(cls):
        return cls.b_class_retrieve_val("im_used")

    @classmethod
    def b_class_register_key_val(cls, key, val):
        class_register = cls.registrable_key_val_dict.get(cls, {})
        class_register[key] = val
        cls.registrable_key_val_dict[cls] = class_register

    @classmethod
    def b_class_retrieve_val(cls, key):
        if cls in cls.registrable_key_val_dict and key in cls.registrable_key_val_dict[cls]:
            return cls.registrable_key_val_dict[cls][key]
        else:
            return None

    @classmethod
    def b_class_set_relevant_output_file(cls, val):
        cls.b_class_register_key_val("_output_file", val)

    @classmethod
    def b_class_get_relevant_output_file(cls, must_be_set=True):
        res = cls.b_class_retrieve_val("_output_file")
        if must_be_set:
            assert res, "Not relevant output file is registered. Maybe a probleme in scripts pool ordering?"
        return res

    ## the following functions must be redefined in the derived classes
    def proper_init(self):
        pass

    def b_finalise_script(self):
        pass


class DummyScript(BaseScript):
    """a script which does nothing
    This class is used just to create an extra psedo job
    when only one is present, and thus create at least one edge
    """

    def proper_init(self):
        pass

    def b_finalise_script(self):
        s = f"""echo   "I am a dummy script and I do nothing. I'm just a place holder.  " """
        self.b_add_script_line(s)


class BasePar:
    # these methods are common to all derived classes and in principle they
    # dont necesserely need to be changed. This class is the base class for derived classes.
    # Each derived class has, itself, members of other derived classes of BasePar
    #
    global_histories = {}
    max_history_len = 20
    b_my_histories_add_to = []
    b_my_histories_take_from = []
    b_neuralgic_id = None
    b_is_root = False
    b_auxiliary_global_parameters = {}
    value = None
    last_added_inode_num = None
    tacit = False
    punctiliousness_handle = "no_punctiliousness_control_as_default_for_user_interaction. Redefined this tag to match one of the used handles in the User Serialiser (FromUser serialiser)"
    def b_get_last_added_inode_num(self):
        # if self.last_added_inode_num is None:
        #     return  -1
        # else:
            return self.last_added_inode_num
    punctiliousness_offset = 0

        
    def b_next_inode_num(self):
            return self.last_added_inode_num + 1

    def b_get_session_name(self):
        if not self.b_is_root:
            raise RuntimeError(""" Session name can only be asked for root node """)
        for member in self.all_members:
            if isinstance(member, SessionNamePar):
                return member.get_session_name()
        else:
            raise RuntimeError(
                "get_session_name was called by the node does not contain any subnode of class SessionNamePar"
            )

    def b_workflow_build(self, script_configuration_list, session_name=None):
        if session_name is None:
            if self.b_is_root:
                session_name = self.b_get_session_name()  # self must be root here or it will fail
                self.b_auxiliary_global_parameters["session_name"] = session_name
            else:
                session_name = self.b_auxiliary_global_parameters["session_name"]

        for script_configuration in script_configuration_list:
            script_configuration.b_start_considering_neuralgic_node(self)
            script_configuration.b_consider_neuralgic_node(self)

        for my_member in self.all_members:
            my_member.b_workflow_build(script_configuration_list)

        if self.b_neuralgic_id is not None:
            for script_configuration in script_configuration_list:
                script_configuration.b_stop_considering_neuralgic_node(self)

    def get_completions(self):
        result = []
        for hname in self.b_my_histories_take_from:
            if hname in self.global_histories:
                for line in self.global_histories[hname]:
                    if line not in result:
                        result.append(line)

        for line in self.completion_history:
            if line not in result:
                result.append(line)

        return result

    def store_completion(self, line):
        self.completion_history.append(line)
        for hname in self.b_my_histories_add_to:
            h = self.global_histories.get(hname, [])
            if line in h:
                h.remove(line)
            h.append(line)
            h = h[-self.max_history_len :]
            self.global_histories[hname] = h

    def __init__(self, serialisation_object=None):
        self.all_members = []
        self.question_answer_dict = {}
        self.completion_history = []
        if serialisation_object is not None:
            self.serialise_or_new(serialisation_object)

    def configure_scheme(self, skip_to=None,  go_to=None, global_histories=None, user_serialiser=None, automatic=False):
        """to be used for the general, preliminary, configuration"""
        if user_serialiser is None:
            serialisation_object = FromUser(skip_to=skip_to, go_to=go_to, automatic=automatic)
        else:
            serialisation_object = user_serialiser
            
        if global_histories is not None:
            self.global_histories.update( global_histories)
            
        self.serialise_or_new(serialisation_object)

    def configure_run(self):
        """To be used before launching jobs, allow to finalise the run"""
        self.proper_configure_run()
        for my_member in self.all_members:
            my_member.configure_run()

    def serialise_forward(self, serialisation_object):
        serialisation_object.set_class_name(self.__class__.__name__)
        serialisation_object.set_question_answer_dict(self.question_answer_dict)
        serialisation_object.set_completion_history(self.completion_history)

        if serialisation_object.is_root:
            serialisation_object.set_global_histories(self.global_histories)
            serialisation_object["__version__"] = self.__version__

        for my_member in self.all_members:
            new_node = serialisation_object.make_new_node()
            my_member.serialise_forward(new_node)

    def serialise_or_new(self, serialisation_object):
        if not isinstance(serialisation_object, (SerialiseBackward, FromDefault, FromUser, SerialiseForward)):
            message = f""" The parameters object* is not being used with the proper class of serialiser. it was {serialisation_object} """

        if isinstance(serialisation_object, (SerialiseBackward, FromDefault, FromUser)):
            if isinstance(serialisation_object, SerialiseBackward):
                if "__version__" in serialisation_object:
                    if serialisation_object["__version__"] != self.__version__:
                        message = f"""The loaded configuration has version {serialisation_object["__version__"]} which does not match with the library version {self.__version__} """
                        logger.warning(message)

            if isinstance(serialisation_object, SerialiseBackward):
                if self.b_is_root:
                    self.global_histories.update(serialisation_object["global_histories"])
            self.b_proper_init(serialisation_object)
        else:
            self.serialise_forward(serialisation_object)

            ## Save, in the reimplementation not here, your objects
            ## self.dataset_location.dump(serialisation_object)

    def b_set_or_create_member(self, inode=0, cls=None, parent_serialiser=None):

        if parent_serialiser.just_one_shot:
            self.last_added_inode_num = inode
            return
                
        new_serial_object = parent_serialiser.get_subnode(inode, cls.__name__)
        self.last_added_inode_num = inode

        if inode < len(self.all_members):
            tentative_object = self.all_members[inode]
            if not tentative_object.__class__ == cls:
                tentative_object = None
        else:
            assert inode == len(self.all_members)
            self.all_members.append(None)
            tentative_object = None

        if tentative_object is None:
            tentative_object = cls(new_serial_object)
            self.all_members[inode] = tentative_object
        else:
            tentative_object.b_proper_init(new_serial_object)
        return tentative_object

    def b_proper_init(self, serialisation_object):
        if isinstance(serialisation_object, FromUser):
            serialisation_object.my_current_level += 1
            self.proper_init(serialisation_object)
            serialisation_object.my_current_level -= 1
        else:
            self.proper_init(serialisation_object)

    # these others, even if common to all derived classe needs to be adapted/reimplemented
    def proper_init(self, serialisation_object):
        raise RuntimeError("This function must be reimplemented in the derived classes")

    def proper_serialise_forward(self, serialisation_object):
        raise RuntimeError("This function must be reimplemented in the derived classes")

    def proper_configure_run(self):
        raise RuntimeError("This function must be reimplemented in the derived classes")


    def b_ask_input_from_question(
        self,
        question,
        serialisation_object,
        answer_dict,
        default,
        autocompletion=False,
        type_to=False,
        typed_default=None,
        path_is_dir=False,
        path_is_file=False,
        create_dir_on_demand=False,
    ):
        # Addeth the query to thy noble question
        question = question + ": "
        if isinstance(serialisation_object, FromDefault) or self.tacit:
            # Should the object be of default nature or taciturn, taketh the default answer
            answer = default
    
        elif isinstance(serialisation_object, FromUser):
            # If the user musteth respond to the call of input:
            if serialisation_object.automatic:
                # Should this be automatic, herald the answer, bypassing mortal interference
                print(question + "\n" + "  automatically skipped --> ", default)
                answer_dict[question] = default
                return default
    
            elif serialisation_object.skip_to is not None:
                # Should the query merit skipping, thus declareth it skipped
                if serialisation_object.skip_to in question:
                    serialisation_object.skip_to = None
                else:
                    print(question + "\n" + "       skipped --> ", default)
                    return default
    
            elif serialisation_object.go_to is not None:
                # Directeth the flow toward a specific goal or else proceedeth forthwith
                if serialisation_object.go_to in question:
                    pass  # Let it continue in its duty, yet unset it not
                else:
                    print(question + "\n" + "       skipped --> ", default)
                    return default
    
            # Pose the question anew unto the user for an answer
            self._asked_question = question
            if not autocompletion and not type_to:
                answer = serialisation_object.input_from_question_answer(question, default=default, asker=self)
            else:
                # Engage the sagacity of autocompletion to proffer a response
                completion_history = self.get_completions()
                answer, _ = serialisation_object.input_from_question_answer_and_autocompletion(
                    question,
                    default=default,
                    history=completion_history,
                    type_to=type_to,
                    typed_default=typed_default,
                    path_is_dir=path_is_dir,
                    path_is_file=path_is_file,
                    create_dir_on_demand=create_dir_on_demand,
                    asker=self,
                )
                self.store_completion(answer)
    
        elif isinstance(serialisation_object, SerialiseBackward):
            # Looketh back upon answers of the past and bringeth forth the appropriate one
            answer = serialisation_object.get_previous_answer(question, self.__class__.__name__)
            self.completion_history = serialisation_object.get_completion_history()
        elif isinstance(serialisation_object, SerialiseForward):
            # Inscribeth the answer unto the annals of forward serialization
            if question in answer_dict:
                serialisation_object.set_answer(question, answer_dict[question], history=self.completion_history)
            else:
                serialisation_object.set_answer(question, default, history=self.completion_history)
        else:
            # If all paths fail, lamenteth with confusion
            assert False, " This should not happen"
    
        # Recordeth the answer in the sacred ledger and returneth it
        answer_dict[question] = answer
        return answer



def split_in_possible_choices(question):
    tokens = re.split("[, :]+", question)

    # matches [N]ear  [f]ar [Y] etc. etc.
    full_re = re.compile("\[[a-zA-Z]?\][a-zA-Z]*")

    # matches [N]  [f] [y] etc.etc
    short_re = re.compile("\[[a-zA-Z]?\]")

    possible_choices = {}
    for tok in tokens:
        match = full_re.match(tok)
        if match is None:
            continue
        matched = tok[match.start() : match.end()]
        if len(matched):
            short_match = short_re.match(matched)

            short_form = matched[short_match.start() : short_match.end()].replace("[", "").replace("]", "")
            long_form = matched.replace("[", "").replace("]", "")
            possible_choices.update({short_form.lower(): short_form.lower(), long_form.lower(): short_form.lower()})

    return possible_choices
    #

def simple_input_from_question_answer(question="question for the [U]ser", default="", type=str):
    default = default.lower()
    possible_choices = split_in_possible_choices(question)
    got_answer = False
    while not got_answer:
        if (default != "") and (default not in possible_choices):
            logger.warning(
                f"""The default value "{default}" was not between the possible choices {list(possible_choices.keys())}. Now setting it to empty string"""
            )
            default = ""
        shown_question = question + f":[{default}]"

        answer = input(shown_question).lower()

        if answer == "":
            answer = default
        if answer in possible_choices:
            answer = possible_choices[answer]
            got_answer = True
        else:
            logger.warning(
                f"""The answer that you gave "{answer}" was not between the possible choices {list(possible_choices.keys())}. """
            )
    return answer
        


class TypedPar(BasePar):
    value = "0.0"
    b_my_histories_add_to = []
    b_my_histories_take_from = []
    asked_question = "input a value "
    typed_default = None
    my_type = float

    def proper_init(self, serialisation_object):
        question = self.asked_question
        if self.typed_default is not None:
            question +=  f"(Tab will reset to default value {self.typed_default})"

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
            autocompletion=True,
            type_to=self.my_type,
            typed_default=self.typed_default,
        )


class LocationByPath(BasePar):
    value = "./"
    b_my_histories_add_to = ["bliss_path"]
    b_my_histories_take_from = ["bliss_path"]
    question = "Choose bliss dataset  by input and autocompletion: "
    path_is_dir = False
    path_is_file = False
    create_dir_on_demand = False

    def proper_init(self, serialisation_object):
        self.value = self.b_ask_input_from_question(
            question=self.question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
            autocompletion=True,
            path_is_dir=self.path_is_dir,
            path_is_file=self.path_is_file,
            create_dir_on_demand=self.create_dir_on_demand,
        )


class SessionNamePar(TypedPar):
    value = "unnamed_session"
    b_my_histories_add_to = ["session_name"]
    b_my_histories_take_from = ["session_names"]
    asked_question = "Give a name for this session ( must be contiguous string)"
    typed_default = "unnamed_session"
    my_type = ContiguousString

    def get_session_name(self):
        return self.value


def save_conf_and_glossary(conf_dict, session_name, cwd):
    file_name = os.path.join(cwd, session_name + "_configuration.json")

    with open(file_name, "w") as fp:
        json.dump(conf_dict, fp, indent=4)

    aiguillage = {}
    path = []
    variables_dict = {}
    path_variable = []
    visit_items_for_aiguillage(conf_dict, aiguillage, path, variables_dict, path_variable)

    file_name = os.path.join(cwd, session_name + "_aiguillage.py")

    # with open( file_name, 'w') as fp:
    #     json.dump(aiguillage, fp, indent=4)

    json_name_configuration = session_name + "_configuration.json"

    with open(file_name, "w") as fp:
        fp.write("from functools import reduce\nimport operator\nimport re\nclass Parameters:\n")
        for k, val in variables_dict.items():
            fp.write(f"    {k}  = {val}\n")
        fp.write(f"    all_variables  = {list(variables_dict.keys())}\n")
        fp.write(f"    json_filename  = '{json_name_configuration}'\n")

        # to_be_moved = list(set( BaseScript.move_list_for_processed_dir))
        to_be_moved = list(set(list(BaseScript.to_be_moved)))

        fp.write(f"    to_be_moved  = {to_be_moved}\n")

        fp.write(code_inset_for_dict_change)
        fp.write(code_inset_to_check_var)
        fp.write(code_inset_for_par_change)
        fp.write(code_inset_for_data_movement)
        
    metadata = {}
    json_name_metadata = session_name + "_nr_metadata.json"

    visit_items_for_metadata(conf_dict, metadata)
    with open(json_name_metadata, "w") as f:
        json.dump(metadata, f, indent=4)


code_inset_for_data_movement = """        
if __name__ == "__main__":
    import sys
    import shutil
    import os
    assert (len(sys.argv)==3)
    assert (sys.argv[1]=="move_to")
    target_dir = sys.argv[2]

    for file_name in Parameters.to_be_moved:
        shutil.move( file_name, os.path.join(target_dir,file_name)) 
"""

code_inset_for_dict_change = """        
    @classmethod
    def change_dic_by_parameters( cls, dizio):
        for variable_name in cls.all_variables:
            value, path = getattr(cls, variable_name)
            q_and_A_dict = reduce(operator.getitem, path, dizio)            
            q_and_A_dict[ list(q_and_A_dict.keys())[0]  ] = value
"""

code_inset_for_par_change = """        
    @classmethod
    def search_and_change( cls, name_fragment, value, fragment = None):
        found = []
        for variable_name in cls.all_variables:
            if re.search(name_fragment, variable_name):
                found.append( variable_name )
        if len(found) != 1:
            message = f"Error only one variable name must match the fragment. I found these matches instead : {found}"
            raise ValueError(message)
        full_name = found[0]

        to_be_changed = list(getattr(cls, full_name))

        if fragment is None:
            to_be_changed[0] = str(value)
        else:
            to_be_changed[0] = to_be_changed[0].replace( fragment , str(value)  )
        
        setattr(cls, full_name, to_be_changed )
"""

code_inset_to_check_var = """        
    @classmethod
    def has_variable( cls, name_fragment):
        found = []
        for variable_name in cls.all_variables:
            if re.search(name_fragment, variable_name):
                found.append( variable_name )
        return len(found) > 0
"""


def visit_items_for_aiguillage(conf, aiguillage, path, variables_dict, path_variable):
    aiguillage[conf["class_name"]] = {}
    aiguillage = aiguillage[conf["class_name"]]

    path_variable = path_variable + [conf["class_name"]]

    key_list = list(conf.keys())

    has_node = False
    for k in key_list:
        if k.startswith("node_"):
            if k[len("nodes_") :].isnumeric():
                has_node = True                
                visit_items_for_aiguillage(conf[k], aiguillage, path + [k], variables_dict, path_variable)

    key = "question_answer_dict"
    if len(conf[key]):
        aiguillage[conf["class_name"]] = (list(conf[key].values())[0], path + [key])

        new_variable_name = "_".join(path_variable)
        variables_dict[new_variable_name] = aiguillage[conf["class_name"]]

def visit_items_for_metadata(conf, metadata):
    metadata[conf["class_name"]] = {}
    metadata = metadata[conf["class_name"]]

    key_list = list(conf.keys())

    has_node = False
    for k in key_list:
        if k.startswith("node_"):
            if k[len("nodes_") :].isnumeric():
                has_node = True
                visit_items_for_metadata(conf[k], metadata)

    key = "question_answer_dict"
    if len(conf[key].values()):
        metadata["question"] = list(conf[key].keys())[0]
        metadata["answer"] = list(conf[key].values())[0]

        
def flatten_dict(d, parent_key='', sep='.'):
    """
    Flattens a dictionary keeping only the atomic keys that point to non-dictionary values.

    :param d: Dictionary to flatten
    :param parent_key: Parent key (for recursive use)
    :param sep: Separator between keys
    :return: Flattened dictionary with atomic keys
    """
    items = []
    for k, v in d.items():
        #new_key = f"{parent_key}{sep}{k}" if parent_key else k
        new_key =  k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)
