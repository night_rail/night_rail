import os
import re
import readline


class Completer(object):
    def __init__(self, previous_history=[], type_to=False, typed_default=None):
        self.set_history(previous_history)
        readline.set_completer_delims(" \t\n;")
        readline.parse_and_bind("tab: complete")

        self.type_to = type_to
        self.typed_default = str(typed_default)

        if not type_to or typed_default is not None:
            readline.set_completer(self.complete)

    def set_history(self, previous_history):
        readline.clear_history()
        self.add_history(previous_history)

    def add_history(self, previous_history):
        readline.clear_history()
        for line in previous_history:
            readline.add_history(line)

    def get_history(self):
        result = []
        for i in range(readline.get_current_history_length()):
            token = readline.get_history_item(i + 1)
            if token not in ["ff", "ee"]:
                result.append(token)
        return result

    def _listdir(self, root):
        res = []
        for name in os.listdir(root):
            path = os.path.join(root, name)
            if os.path.isdir(path):
                name += os.sep
            res.append(name)
        return res

    def _complete_path(self, path=None):
        "Perform completion of filesystem path."
        if not path:
            return self._listdir(".")
        dirname, rest = os.path.split(path)
        tmp = dirname if dirname else "."
        res = [os.path.join(dirname, p) for p in self._listdir(tmp) if p.startswith(rest)]
        # more than one match, or single match which does not exist (typo)
        if len(res) > 1 or not os.path.exists(path):
            return res
        # resolved to a single directory, so return list of files below it
        if os.path.isdir(path):
            return [os.path.join(path, p) for p in self._listdir(path)]
        # exact file match terminates this completion
        return [path + " "]

    def complete_extra(self, args):
        "Completions for the 'extra' command."
        if not args:
            return self._complete_path(".")
        # treat the last arg as a path and complete it
        return self._complete_path(args[-1])

    def complete(self, text, state):
        "Generic readline completion entry point."

        if not self.type_to:
            buffer = readline.get_line_buffer()
            line = readline.get_line_buffer().split()
            return (self.complete_extra(line) + [None])[state]
        else:
            return ([self.typed_default, None])[state]
