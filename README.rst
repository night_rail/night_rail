night_rail
==========

The name is inspired by the ability of a rail to guide you to the hoped destination even when the light fades away. This is a methaphore of common situations that are encountered when the transition from raw data to the wished result involves many steps. Each step can be realised by  very advanced algorithms that you are able to improve, then, when everything is ready, you still need to chain everything together in many different ways that depend on the acquisition modes and parameters. The tracking of all the different treatement sequences,  let alone the adaptation of the sequences to new features, is often a boring and error-prone tasks, the overall view on which is easy to loose, expecially after or during a long experiment, expecially at night. 

The library provides two classes:

  - BasePar
  - BaseScript

BasePar can be derived into new classes, one for each parameter, adding some information on the question to ask, and which other parameter to activate according to the answer. A neuralgic identity can be optionally defined  for each parameter.

The scripts, derived from BaseScript, are listening to the parameters, and can activate themselves according to the neuralgic identity, the class type, and the parameter value.
A full fledged example can be found at  http://gitlab.esrf.fr/night_rail/applications/mirone/night_rail_bm18

Here instead a simple application which:

  - asks for a file name
  - let you choose between counting the lines or print the containt
  - build the workflow

The following code is an autostructuring dialog, it will write in a structured json file the decisional tree corresponding to your choice. We need to run this first, then the instruction for building the workflow will follow. Copy the followinf code into *test_par.py* and run it

.. code-block:: python

 import json
 import night_rail
 import os
 
 class RootPar(night_rail.BasePar):
     b_is_root = True
     __version__="0.1"
     def proper_init(self, serialisation_object):
         self.b_set_or_create_member( inode=0, cls =FileLocationPar , parent_serialiser = serialisation_object)
         self.b_set_or_create_member( inode=1, cls =ActionPar , parent_serialiser = serialisation_object)
	 
         # always end the root node containt with the session name
         self.b_set_or_create_member( inode=2, cls =night_rail.SessionNamePar , parent_serialiser = serialisation_object)
    
 class FileLocationPar(night_rail.LocationByPath):
     b_neuralgic_id = "bliss_unique_scan"
     question = "Choose filename  by input and autocompletion: "
     path_is_file= True
 
 class ActionPar(night_rail.BasePar):    
     possible_actions = ["[C]ount lines",  "show [T]ail", "show [H]ead" ]
     value = "c"
     b_neuralgic_id = "action"
 
     def proper_init(self, serialisation_object):
 
         question = "Choose Action: "+(", ".join(self.possible_actions))
         self.value = self.b_ask_input_from_question(
             question = question,
             serialisation_object = serialisation_object,
             answer_dict = self.question_answer_dict,
             default = self.value
         )
         if self.value in  [ "t" , "h" ]:
             self.b_set_or_create_member( inode=0, cls = NOfLinesPar, parent_serialiser = serialisation_object)
 
 class NOfLinesPar(night_rail.TypedPar):
     """ tab to retrieve the default, up-down arrow to scroll the history """
     value = 10
     asked_question =  "Give the number of lines to display  "
     typed_default = 10
     my_type = night_rail.limited_typed_class(int, 1, 100000)
 
 if __name__ == "__main__":
         
     if os.path.exists("previous.json"):
       previous_dict = json.load( open("previous.json","r"))
       my_serialiser = night_rail.SerialiseBackward(previous_dict)
       # reload previous choices
       my_pars = RootPar( my_serialiser )
     else:
       my_pars = RootPar(  )
 
     # user interaction
     my_pars.configure_scheme()
 
     # saving parameters
     my_serialiser = night_rail.SerialiseForward()
     my_pars.serialise_forward(my_serialiser)
     with open("previous.json", 'w') as fp:
       json.dump(my_serialiser, fp, indent=4)

        

Now the building of the workflow follows. We start from a pool of script object which will listen to the decisional tree values, when the night_rail module plays it. Copy the following code into test_script.py. And Run it. The workflow below is quite simple, consisting of only one action. When several scripts are present in the pool ( always in interesting cases) the workflow graph is detemined by the corresponances between required inputs and provided outputs

.. code-block:: python
		
 import json
 import night_rail
 import test_par
 
 class SimpleScript(night_rail.BaseScript):
     """ This script performs a very simple task
     """
     def proper_init(self):
 
         classes_of_interest = [
             test_par.ActionPar,
             test_par.FileLocationPar,
             test_par.NOfLinesPar,
         ]
         self.b_add_to_classes_of_interest( classes_of_interest  )
 
         self.b_add_to_neuralgic_listening_list({ "action"   : self.just_for_demo  })
 
     def just_for_demo(self, par_obj):
         """ this is activated by the neuralgic_id of ActionPar
         but we dont need it here. We are already listening to the class type ActionPar
         """
         pass
 
     def b_finalise_script(self):
 
         """ The workflow graph is detemined by the corresponances
          between required inputs and provided outputs
          This workflow is too simple, otherwise the provided arguments
          belows would be list of filenames , not empty lists
        """
         self.b_extend_required_inputs([])
         self.b_extend_provided_outputs([])
 
         self.b_reset_script_empty()
 
         # add a line
         if self.action_par == "c":
             s = (f"wc -l {self.file_location_par}  ")
         elif  self.action_par == "t":
             s = (f"tail -n {self.n_of_lines_par} {self.file_location_par}  ")
         else:
             s = (f"head -n {self.n_of_lines_par} {self.file_location_par}  ")
 
         self.b_add_script_line( s )
 
 if __name__ == "__main__":
 
     all_scripts = night_rail.BaseScriptsContainer()
     all_scripts . extend( [ SimpleScript(), ])
 
     previous_dict = json.load( open("previous.json","r") )
 
     my_serialiser = night_rail.SerialiseBackward(previous_dict)
 
     my_pars = test_par.RootPar( my_serialiser )
     
     my_pars.b_workflow_build( all_scripts)
     
     all_scripts.b_dump_scripts( session_name = my_pars.b_get_session_name(), version = previous_dict["__version__"], nr_json_file = "previous.json"  )


Running this script you get the materialisation of the workflow ::

 unnamed_session.nro
 unnamed_session.nri
 unnamed_session.nrd
 unnamed_session_ewoks_wf.nrw
 unnamed_session_001_DummyScript.nrs
 unnamed_session_000_SimpleScript.nrs


The dummy script is just a place holder to create at least an edge in the graph ( the example is too simple).
The files postfixed by *nrw* is an ewoks worflow.
It can be runned by ::

  ewoks execute unnamed_session_ewoks_wf.nrw

The files postfixed by *nrs* are the scripts. The postfixes *nrd*, *nro*, *nri* stand for dependencies, outputs and inputs. With the latter two one will be able to automatically detect problem in the form of missing dependencies ( required inputs ot required outputs).
More knowledge can be learnt looking at full fledged examples or directly in the nigh_rail module which is quite short in reality.
